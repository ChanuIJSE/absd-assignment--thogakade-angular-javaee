import { Component, OnInit } from '@angular/core';
import {CustomerService} from "../core/services/customer.service";
import {CustomerDto} from "../core/dto/customer-dto";

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  customerId: string;
  firstName: string;
  lastName: string;
  address: string;
  teleHome: string;
  teleMobile: string;
  email: string;

  constructor(private customerService: CustomerService) { }

  ngOnInit() {
  }

  saveCustomer(){

    let customerDto = new CustomerDto();

    customerDto.customerId = this.customerId;
    customerDto.firstName = this.firstName;
    customerDto.lastName = this.lastName;
    customerDto.address = this.address;
    customerDto.teleHome = this.teleHome;
    customerDto.teleMobile = this.teleMobile;
    customerDto.email = this.email;

    // console.log(this.customerId+" ~ "+this.firstName+" ~ "+this.lastName+" ~ "+this.address+" ~ "+this.teleHome+" ~ "+this.teleMobile+" ~ "+this.email);

    this.customerService.saveCustomer(customerDto).subscribe( data => {
      console.log(data);
      if (data) {
        alert("Successfully Added...!");
      } else {
        alert("Not added...!");
      }
    });

  }

}
