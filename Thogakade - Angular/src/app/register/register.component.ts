import { Component, OnInit } from '@angular/core';
import {UserService} from "../core/services/user.service";
import {UserDto} from "../core/dto/user-dto";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  username: string;
  password: string;
  confirmPassword: string;

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  registerUser() {
    if (this.password !== this.confirmPassword) {
      alert("Password & confirm password are not matched...!!!")
    } else {

      let userDto = new UserDto();

      userDto.username = this.username;
      userDto.password = this.password;

      this.userService.saveUser(userDto).subscribe( data => {
        console.log(data);
      });

    }
  }

}
