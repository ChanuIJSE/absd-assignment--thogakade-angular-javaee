import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {UserDto} from "../dto/user-dto";
import {Observable} from "rxjs";

export const MAIN_URL = 'http://localhost:8080';
const USER_URL = '/login';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpClient: HttpClient) { }

  logInUser(user: UserDto): Observable<boolean> {
    return this.httpClient.post<boolean>(MAIN_URL + USER_URL, user);
  }

}
