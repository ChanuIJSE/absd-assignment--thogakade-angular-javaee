import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {OrderDto} from "../dto/order-dto";

export const MAIN_URL = 'http://localhost:8080';
const ORDER_URL = '/api/customer/save-customer';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private httpClient: HttpClient) { }

  saveOrder(order: OrderDto): Observable<boolean> {
    console.log("Order: "+order.orderId+" ~ "+order.amount);
    return this.httpClient.post<boolean>(MAIN_URL + ORDER_URL, order);
  }

}
