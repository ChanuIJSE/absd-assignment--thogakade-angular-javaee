export class OrderDto {
  orderId: string;
  customerId: string;
  itemCode: string;
  orderQty: number;
  amount: number;
}
