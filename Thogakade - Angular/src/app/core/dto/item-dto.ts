export class ItemDto {
  itemCode: string;
  itemName: string;
  description: string;
  unitPrice: number;
  qty: number;
  sellerEmail: string;
}
