export class UserDto {
  username: string;
  password: string;

  constructor(init?: Partial<UserDto>) {
    Object.assign(this, init);
  }

}
