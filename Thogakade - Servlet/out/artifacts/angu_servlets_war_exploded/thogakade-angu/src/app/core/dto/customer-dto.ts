export class CustomerDto {
  customerId: string;
  firstName: string;
  lastName: string;
  address: string;
  teleHome: string;
  teleMobile: string;
  email: string;
}
