import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ItemDto} from "../dto/item-dto";

export const MAIN_URL = 'http://localhost:8080';
const ITEM_URL = '/api/item';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  constructor(private httpClient: HttpClient) { }

  saveItem(item: ItemDto): Observable<boolean> {
    console.log("Item: "+item.itemCode+" ~ "+item.itemName);
    return this.httpClient.post<boolean>(MAIN_URL + ITEM_URL, item);
  }

  getAllItems():Observable<Array<ItemDto>>{
    return this.httpClient.get<Array<ItemDto>>(MAIN_URL + ITEM_URL);
  }

}
