import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {CustomerDto} from "../dto/customer-dto";

export const MAIN_URL = 'http://localhost:8080';
const CUSTOMER_URL = '/api/customer';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private httpClient: HttpClient) { }

  saveCustomer(customer: CustomerDto): Observable<boolean> {
    console.log("Customer: "+customer.customerId+" ~ "+customer.firstName);
    return this.httpClient.post<boolean>(MAIN_URL + CUSTOMER_URL, customer);
  }

  getAllCustomers():Observable<Array<CustomerDto>>{
    return this.httpClient.get<Array<CustomerDto>>(MAIN_URL + CUSTOMER_URL);
  }

}
