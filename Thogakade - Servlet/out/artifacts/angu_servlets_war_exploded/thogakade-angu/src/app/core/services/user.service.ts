import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {UserDto} from "../dto/user-dto";

export const MAIN_URL = '';
const USER_URL = '/api/user/register-user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }

  saveUser(user: UserDto): Observable<boolean> {
    console.log("User: "+user.username+" ~ "+user.password);
    return this.httpClient.post<boolean>(MAIN_URL + USER_URL, user);
  }

}
