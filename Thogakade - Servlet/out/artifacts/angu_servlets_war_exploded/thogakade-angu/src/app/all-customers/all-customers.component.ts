import { Component, OnInit } from '@angular/core';
import {CustomerService} from "../core/services/customer.service";
import {CustomerDto} from "../core/dto/customer-dto";

@Component({
  selector: 'app-all-customers',
  templateUrl: './all-customers.component.html',
  styleUrls: ['./all-customers.component.css']
})
export class AllCustomersComponent implements OnInit {

  customers: CustomerDto[] = [];

  constructor(private customerService: CustomerService) { }

  ngOnInit() {
    this.getAllCustomers();
  }

  private getAllCustomers(){
    this.customerService.getAllCustomers().subscribe(data =>{
      this.customers = data;
      console.log(data);
    });
  }

}
