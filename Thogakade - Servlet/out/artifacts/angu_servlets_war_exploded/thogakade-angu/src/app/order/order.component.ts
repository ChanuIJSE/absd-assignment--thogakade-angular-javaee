import { Component, OnInit } from '@angular/core';
import {OrderDto} from "../core/dto/order-dto";
import {OrderService} from "../core/services/order.service";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  orderId: string;
  customerId: string;
  itemCode: string;
  orderQty: number;
  amount: number;

  constructor(private orderService: OrderService) { }

  ngOnInit() {
  }

  saveOrder(){
    // console.log(this.orderId+" ~ "+this.customerId+" ~ "+this.itemCode+" ~ "+this.orderQty+" ~ "+this.amount);
    let orderDto = new OrderDto();

    orderDto.orderId = this.orderId;
    orderDto.customerId = this.customerId;
    orderDto.itemCode = this.itemCode;
    orderDto.orderQty = this.orderQty;
    orderDto.amount = this.amount;

    this.orderService.saveOrder(orderDto).subscribe(data => {
      console.log(data);
    });

  }

}
