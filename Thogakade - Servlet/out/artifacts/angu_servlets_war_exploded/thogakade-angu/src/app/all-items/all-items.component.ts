import { Component, OnInit } from '@angular/core';
import {ItemService} from "../core/services/item.service";
import {CustomerDto} from "../core/dto/customer-dto";
import {ItemDto} from "../core/dto/item-dto";

@Component({
  selector: 'app-all-items',
  templateUrl: './all-items.component.html',
  styleUrls: ['./all-items.component.css']
})
export class AllItemsComponent implements OnInit {

  items: ItemDto[] = [];

  constructor(private itemService: ItemService) { }

  ngOnInit() {
    this.getAllItems();
  }

  getAllItems(){
    this.itemService.getAllItems().subscribe(data => {
      this.items = data;
      console.log(data);
    });
  }

}
