import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';
import {CustomerComponent} from './customer/customer.component';
import {ItemComponent} from './item/item.component';
import {OrderComponent} from './order/order.component';
import {AllCustomersComponent} from './all-customers/all-customers.component';
import {AllItemsComponent} from './all-items/all-items.component';
import {RegisterComponent} from './register/register.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', pathMatch: 'full', redirectTo: '/login' },
  { path: 'home', component: HomeComponent },
  { path: 'customer', component: CustomerComponent },
  { path: 'item', component: ItemComponent },
  { path: 'order', component: OrderComponent },
  { path: 'all-customers', component: AllCustomersComponent },
  { path: 'all-items', component: AllItemsComponent },
  { path: 'register', component: RegisterComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
