import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { CustomerComponent } from './customer/customer.component';
import { ItemComponent } from './item/item.component';
import { OrderComponent } from './order/order.component';
import { AllCustomersComponent } from './all-customers/all-customers.component';
import { AllItemsComponent } from './all-items/all-items.component';
import { RegisterComponent } from './register/register.component';
import { FormsModule } from '@angular/forms';
import {CustomerService} from "./core/services/customer.service";
import {ItemService} from "./core/services/item.service";
import {OrderService} from "./core/services/order.service";
import {UserService} from "./core/services/user.service";
import {HttpClientModule} from '@angular/common/http'

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    LoginComponent,
    HomeComponent,
    CustomerComponent,
    ItemComponent,
    OrderComponent,
    AllCustomersComponent,
    AllItemsComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    CustomerService,
    ItemService,
    OrderService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
