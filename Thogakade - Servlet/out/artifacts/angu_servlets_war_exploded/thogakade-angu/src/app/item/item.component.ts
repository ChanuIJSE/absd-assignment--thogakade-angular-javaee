import { Component, OnInit } from '@angular/core';
import {ItemService} from "../core/services/item.service";
import {ItemDto} from "../core/dto/item-dto";

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  itemCode: string;
  itemName: string;
  description: string;
  unitPrice: number;
  qty: number;
  sellerEmail: string;

  constructor(private itemService: ItemService) { }

  ngOnInit() {
  }

  saveItem(){

    let itemDto = new ItemDto;

    itemDto.itemCode = this.itemCode;
    itemDto.itemName = this.itemName;
    itemDto.description = this.description;
    itemDto.unitPrice = this.unitPrice;
    itemDto.qty = this.qty;
    itemDto.sellerEmail = this.sellerEmail;

    // console.log(this.itemCode+" ~ "+this.itemName+" ~ "+this.description+" ~ "+this.unitPrice+" ~ "+this.qty+" ~ "+this.sellerEmail);

    this.itemService.saveItem(itemDto).subscribe(data => {
      console.log(data);
      if (data) {
        alert("Successfully Added...!");
      } else {
        alert("Not added...!");
      }
    });

  }

}
