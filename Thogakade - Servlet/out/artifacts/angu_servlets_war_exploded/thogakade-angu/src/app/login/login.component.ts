import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {UserDto} from "../core/dto/user-dto";
import {LoginService} from "../core/services/login.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;

  constructor(private router: Router, private loginService: LoginService) { }

  ngOnInit() {
  }

  login(){

    let userDto = new UserDto();

    userDto.username = this.username;
    userDto.password = this.password;

    this.loginService.logInUser(userDto).subscribe( data => {
      console.log(data);
      if (data) {
        this.router.navigateByUrl('/home');
      } else {
        alert("Login Fail...!!!")
      }
    });

  }

}
