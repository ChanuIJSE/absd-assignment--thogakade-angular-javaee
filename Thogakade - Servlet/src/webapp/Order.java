package webapp;

import com.google.gson.Gson;
import webapp.common.Check;
import webapp.dto.OrderDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@WebServlet(name = "Order")
public class Order extends HttpServlet {

    private DataSource dataSource;

    @Override
    public void init() throws ServletException {
        super.init();
        if (dataSource==null) {
            dataSource = (DataSource) getServletContext().getAttribute("dbPool");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Gson gson = new Gson();
        PrintWriter writer = response.getWriter();
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            StringBuilder sb = new StringBuilder();
            String sw;
            while ((sw = request.getReader().readLine()) != null) {
                sb.append(sw);
            }
            OrderDto dto = gson.fromJson(sb.toString(), OrderDto.class);

            connection = dataSource.getConnection();
            statement = connection.prepareStatement("INSERT INTO Orders VALUES (?,?,?,?,?)");
            statement.setString(1,dto.getOrderId());
            statement.setString(2,dto.getItemCode());
            statement.setString(3,dto.getCustomerId());
            statement.setInt(4,dto.getOrderQty());
            statement.setDouble(5,dto.getAmount());

            int rst = statement.executeUpdate();
            Check.checkResult(gson, writer, rst);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
