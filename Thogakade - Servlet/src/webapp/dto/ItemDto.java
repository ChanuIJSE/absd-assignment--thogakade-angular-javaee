package webapp.dto;

public class ItemDto {

    private String itemCode;
    private String itemName;
    private String description;
    private double unitPrice;
    private int qty;
    private String sellerEmail;

    public ItemDto() {
    }

    public ItemDto(String itemCode, String itemName, String description, double unitPrice, int qty, String sellerEmail) {
        this.itemCode = itemCode;
        this.itemName = itemName;
        this.description = description;
        this.unitPrice = unitPrice;
        this.qty = qty;
        this.sellerEmail = sellerEmail;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getSellerEmail() {
        return sellerEmail;
    }

    public void setSellerEmail(String sellerEmail) {
        this.sellerEmail = sellerEmail;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ItemDto{");
        sb.append("itemCode='").append(itemCode).append('\'');
        sb.append(", itemName='").append(itemName).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", unitPrice=").append(unitPrice);
        sb.append(", qty=").append(qty);
        sb.append(", sellerEmail='").append(sellerEmail).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
