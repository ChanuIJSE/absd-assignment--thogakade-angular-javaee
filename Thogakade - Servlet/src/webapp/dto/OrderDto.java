package webapp.dto;

public class OrderDto {

    private String orderId;
    private String itemCode;
    private String customerId;
    private int orderQty;
    private double amount;

    public OrderDto() {
    }

    public OrderDto(String orderId, String itemCode, String customerId, int orderQty, double amount) {
        this.orderId = orderId;
        this.itemCode = itemCode;
        this.customerId = customerId;
        this.orderQty = orderQty;
        this.amount = amount;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public int getOrderQty() {
        return orderQty;
    }

    public void setOrderQty(int orderQty) {
        this.orderQty = orderQty;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("OrderDto{");
        sb.append("orderId='").append(orderId).append('\'');
        sb.append(", itemCode='").append(itemCode).append('\'');
        sb.append(", customerId='").append(customerId).append('\'');
        sb.append(", orderQty=").append(orderQty);
        sb.append(", amount=").append(amount);
        sb.append('}');
        return sb.toString();
    }
}
