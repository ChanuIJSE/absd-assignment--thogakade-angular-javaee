package webapp.dto;

public class CustomerDto {

    private String customerId;
    private String firstName;
    private String lastName;
    private String address;
    private String teleHome;
    private String teleMobile;
    private String email;

    public CustomerDto() {
    }

    public CustomerDto(String customerId, String firstName, String lastName, String address, String teleHome,
                       String teleMobile, String email) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.teleHome = teleHome;
        this.teleMobile = teleMobile;
        this.email = email;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTeleHome() {
        return teleHome;
    }

    public void setTeleHome(String teleHome) {
        this.teleHome = teleHome;
    }

    public String getTeleMobile() {
        return teleMobile;
    }

    public void setTeleMobile(String teleMobile) {
        this.teleMobile = teleMobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CustomerDto{");
        sb.append("customerId='").append(customerId).append('\'');
        sb.append(", firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", address='").append(address).append('\'');
        sb.append(", teleHome='").append(teleHome).append('\'');
        sb.append(", teleMobile='").append(teleMobile).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
