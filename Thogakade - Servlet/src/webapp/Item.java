package webapp;

import com.google.gson.Gson;
import webapp.common.Check;
import webapp.dto.CustomerDto;
import webapp.dto.ItemDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet(name = "Item")
public class Item extends HttpServlet {

    private DataSource dataSource;

    @Override
    public void init() throws ServletException {
        super.init();
        if (dataSource==null) {
            dataSource = (DataSource) getServletContext().getAttribute("dbPool");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Gson gson = new Gson();
        PrintWriter writer = response.getWriter();
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            StringBuilder sb = new StringBuilder();
            String sw;
            while ((sw = request.getReader().readLine()) != null) {
                sb.append(sw);
            }
            ItemDto dto = gson.fromJson(sb.toString(), ItemDto.class);

            connection = dataSource.getConnection();
            statement = connection.prepareStatement("INSERT INTO Item VALUES (?,?,?,?,?,?)");
            statement.setString(1,dto.getItemCode());
            statement.setString(2,dto.getItemName());
            statement.setString(3,dto.getDescription());
            statement.setDouble(4,dto.getUnitPrice());
            statement.setInt(5,dto.getQty());
            statement.setString(6,dto.getSellerEmail());

            int rst = statement.executeUpdate();
            Check.checkResult(gson, writer, rst);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            ArrayList<ItemDto> arrayList = new ArrayList<>();
            connection = dataSource.getConnection();
            statement = connection.prepareStatement("SELECT * FROM Item");

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                ItemDto dto = new ItemDto();
                dto.setItemCode(resultSet.getString(1));
                dto.setItemName(resultSet.getString(2));
                dto.setDescription(resultSet.getString(3));
                dto.setUnitPrice(resultSet.getDouble(4));
                dto.setQty(resultSet.getInt(5));
                dto.setSellerEmail(resultSet.getString(6));
                arrayList.add(dto);
            }

            Gson gson = new Gson();
            String s = gson.toJson(arrayList);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(s);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
