package webapp;

import com.google.gson.Gson;
import webapp.common.Check;
import webapp.dto.CustomerDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet(name = "Customer")
public class Customer extends HttpServlet {

    private DataSource dataSource;

    @Override
    public void init() throws ServletException {
        super.init();
        if (dataSource==null) {
            dataSource = (DataSource) getServletContext().getAttribute("dbPool");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Gson gson = new Gson();
        PrintWriter writer = response.getWriter();
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            StringBuilder sb = new StringBuilder();
            String sw;
            while ((sw = request.getReader().readLine()) != null) {
                sb.append(sw);
            }
            CustomerDto dto = gson.fromJson(sb.toString(), CustomerDto.class);

            connection = dataSource.getConnection();
            statement = connection.prepareStatement("INSERT INTO Customer VALUES (?,?,?,?,?,?,?)");
            statement.setString(1,dto.getCustomerId());
            statement.setString(2,dto.getFirstName());
            statement.setString(3,dto.getLastName());
            statement.setString(4,dto.getAddress());
            statement.setString(5,dto.getTeleHome());
            statement.setString(6,dto.getTeleMobile());
            statement.setString(7,dto.getEmail());

            int rst = statement.executeUpdate();
            Check.checkResult(gson, writer, rst);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            ArrayList<CustomerDto> arrayList = new ArrayList<>();
            connection = dataSource.getConnection();
            statement = connection.prepareStatement("SELECT * FROM Customer");

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                CustomerDto dto = new CustomerDto();
                dto.setCustomerId(resultSet.getString(1));
                dto.setFirstName(resultSet.getString(2));
                dto.setLastName(resultSet.getString(3));
                dto.setAddress(resultSet.getString(4));
                dto.setTeleHome(resultSet.getString(5));
                dto.setTeleMobile(resultSet.getString(6));
                dto.setEmail(resultSet.getString(7));
                arrayList.add(dto);
            }

            Gson gson = new Gson();
            String s = gson.toJson(arrayList);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(s);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
