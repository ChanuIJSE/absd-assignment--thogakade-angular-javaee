package webapp.Listener;

import webapp.db.DBConnection;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.DataSource;

public class ContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext context = sce.getServletContext();
        DataSource source = DBConnection.getConnectionPool();
        context.setAttribute("dbPool",source);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }

}
